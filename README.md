# Devinette d'un nombre 
## 1- Créer un nouveau projet sur Gitlab nommé « Devinette d'un nombre » et le cloner sur votre ordinateur.
![image 1](captures/Question 1.png)
## 2- Créer dans le dossier cloné un fichier « main.py » et écrire le programme du jeu.
![image 2](captures/Question 2.png)
## 3- Propagez les modifications sur le dépôt distant avec un message de commit clair et précis.
![image 3](captures/Question 3.png)
## 4- Créez une nouvelle branche « dev ».
![image 4](captures/Question 4.png)
## 5- Développer dans cette branche la possibilité que le programme affiche des indices pour aider l'utilisateur (plus grand, plus petit) et qu’il affiche à la fin après combien d’essais.
![image 5](captures/Question 5.png)
## 5- Sauvegarder les modifications sur le dépôt distant.
![image 6](captures/Question 5 copie.png)
## 6- Procéder à la fusion de la banche « dev » avec la branche principale.
![image 7](captures/Question 6.png)